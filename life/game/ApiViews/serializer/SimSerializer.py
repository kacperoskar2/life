from rest_framework import serializers

from game.models import Sim


class SimSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sim
        fields = ('pk', 'name', 'surname', 'birthday_date',
                  'satiety', 'health', 'thirst',
                  'happiness', 'account_balance', 'age'
                  )