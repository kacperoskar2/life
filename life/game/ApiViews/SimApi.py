from rest_framework.viewsets import ModelViewSet

from game.ApiViews.serializer.SimSerializer import SimSerializer
from game.models import Sim


class SimApiView(ModelViewSet):
    serializer_class = SimSerializer
    queryset = Sim.objects.all()
