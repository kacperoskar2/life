from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from game.ApiViews.SimApi import SimApiView
from game.views import CreateSimView, GamesList, GameView

router = DefaultRouter()
router.register(r'sim-api', SimApiView)

urlpatterns = [
    url(r'games/new/$', CreateSimView.as_view(), name="create_sim"),
    url(r'games/$', GamesList.as_view(), name="games_list"),
    url(r'games/(?P<pk>[0-9]+)/$', GameView.as_view(), name='game'),
    url(r'', include(router.urls)),
]
