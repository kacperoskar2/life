from datetime import datetime

from dateutil.relativedelta import relativedelta
from django import forms

from game.models import Sim


class CreateSimForm(forms.ModelForm):
    class Meta:
        model = Sim
        fields = ['name',
                  'surname',
                  'birthday_date',
                  'satiety',
                  'health',
                  'thirst',
                  'happiness',
                  'account_balance',
                  ]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fields['birthday_date'].widget = forms.HiddenInput()
        self.fields['satiety'].widget = forms.HiddenInput()
        self.fields['health'].widget = forms.HiddenInput()
        self.fields['thirst'].widget = forms.HiddenInput()
        self.fields['happiness'].widget = forms.HiddenInput()
        self.fields['account_balance'].widget = forms.HiddenInput()

        self.fields['birthday_date'].initial = datetime.now() - relativedelta(years=18)
