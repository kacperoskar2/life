from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView, ListView, DetailView

from game.forms import CreateSimForm
from game.models import Game


class UserMainMenuView(LoginRequiredMixin, TemplateView):
    template_name = 'game/user_main_menu.html'


class CreateSimView(LoginRequiredMixin, CreateView):
    template_name = 'game/create_sim.html'
    form_class = CreateSimForm

    def get_success_url(self):
        Game.objects.create(user=self.request.user, sim=self.object)
        return reverse_lazy("game:games_list")


class GamesList(LoginRequiredMixin, ListView):
    template_name = 'game/games_list.html'
    context_object_name = 'games_list'
    model = Game

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.filter(user=self.request.user)


class GameView(LoginRequiredMixin, DetailView):
    template_name = 'game/game_main.html'
    context_object_name = 'game'
    model = Game

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['sim'] = self.object.sim
        return ctx
