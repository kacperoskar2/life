from django.contrib import admin

from game.models import Game, Sim

admin.site.register(Game)
admin.site.register(Sim)
