from datetime import datetime

from dateutil.relativedelta import relativedelta
from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _

User = get_user_model()


class Game(models.Model):
    user = models.ForeignKey(User,
                             blank=True,
                             null=True,
                             )
    sim = models.OneToOneField('Sim',)

    def __str__(self):
        return self.sim.__str__()


class Sim(models.Model):
    name = models.CharField(_('Name'),
                            max_length=255,
                            )
    surname = models.CharField(_('Surname'),
                               max_length=255,
                               )
    birthday_date = models.DateTimeField(_('Birthday date'),
                                         )
    satiety = models.PositiveSmallIntegerField(_('Satiety'),
                                               validators=[MinValueValidator(0),
                                                           MaxValueValidator(100)],
                                               default=100,
                                               )
    health = models.PositiveSmallIntegerField(_('Health'),
                                              validators=[MinValueValidator(0),
                                                          MaxValueValidator(100)],
                                              default=100,
                                              )
    thirst = models.PositiveSmallIntegerField(_('Thirsty'),
                                              validators=[MinValueValidator(0),
                                                          MaxValueValidator(100)],
                                              default=100,
                                              )
    happiness = models.PositiveSmallIntegerField(_('Happiness'),
                                                 validators=[MinValueValidator(0),
                                                             MaxValueValidator(100)],
                                                 default=100,
                                                 )
    account_balance = models.IntegerField(_('Account balance'),
                                          default=10000)

    def __str__(self):
        return f'{self.name} {self.surname}'

    @property
    def age(self):
        return relativedelta(datetime.today().date(), self.birthday_date.date()).years
