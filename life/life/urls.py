from django.conf.urls import url, include
from django.contrib import admin

from game.views import UserMainMenuView

urlpatterns = [
    url(r'^$', UserMainMenuView.as_view(), name='user_main_menu'),
    url(r'^admin/', admin.site.urls),
    url(r'^users/', include('users.urls', namespace='users')),
    url(r'^game/', include('game.urls', namespace='game')),
]
